const burger = document.querySelector(".burger");
const menu = document.querySelector(".header__nav");

burger.addEventListener("click", () => {
    menu.classList.toggle("menu_active");
    burger.classList.toggle("burger_active");

})
